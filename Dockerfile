FROM ubuntu:20.04
EXPOSE 80
WORKDIR /var/www
ADD https://download.owncloud.com/server/stable/owncloud-complete-latest.tar.bz2 .
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get install -y \
    apache2 \
    libapache2-mod-php \
    php-gd \
    php-json \
    php-mysql \
    php-sqlite3 \
    php-curl \
    php-intl \
    php-imagick \
    php-zip \
    php-xml \
    php-mbstring \
    php-soap \
    php-ldap \
    php-apcu \
    php-redis \
    php-dev \
    libsmbclient-dev \
    php-gmp \
    smbclient --no-install-recommends && rm -rf /var/lib/apt/lists/* && tar -xjf owncloud-complete-latest.tar.bz2
WORKDIR /var/www/owncloud
RUN cp -r ./* /var/www/html && chown -R www-data. /var/www/html
ENTRYPOINT [ "/usr/sbin/apache2ctl" ]
CMD [ "-D", "FOREGROUND" ]
